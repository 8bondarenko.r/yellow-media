<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Symfony\Component\Mime\Encoder\Base64Encoder;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->unique()->lastName,
            'email' => $this->faker->unique()->safeEmail,
            'password' => Hash::make($this->faker->unique()->password),
            'phone' => $this->faker->unique()->phoneNumber,
            'api_token' => (new Base64Encoder())->encodeString(Str::random(8)),
        ];
    }
}
