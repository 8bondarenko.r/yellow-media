<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Service\Company\CompanyService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    /**
     * @var CompanyService
     */
    private $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            return $this->companyService->storeCompany($request);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 401);
        }

    }

    /**
     * @return JsonResponse
     */
    public function show(): JsonResponse
    {
        try {
            return $this->companyService->getCompanyByUserId(Auth::user()->id);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 401);
        }
    }
}
