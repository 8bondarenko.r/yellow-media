<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Service\Register\RegisterService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use \Exception;

class UserRegisterController extends Controller
{
    /**
     * @var RegisterService
     */
    private $registerService;

    /**
     * @param RegisterService $registerService
     */
    public function __construct(RegisterService $registerService)
    {
        $this->registerService = $registerService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        try {
            return $this->registerService->register($request);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 422);
        }
    }
}
