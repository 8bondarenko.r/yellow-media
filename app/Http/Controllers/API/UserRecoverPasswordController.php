<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Service\RecoverPassword\RecoverPasswordService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserRecoverPasswordController extends Controller
{
    /**
     * @var RecoverPasswordService
     */
    private $recoverPasswordService;

    /**
     * @param RecoverPasswordService $recoverPasswordService
     */
    public function __construct(RecoverPasswordService $recoverPasswordService)
    {
        $this->recoverPasswordService = $recoverPasswordService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function recoverPassword(Request $request): JsonResponse
    {
        try {
            return $this->recoverPasswordService->recoverPassword($request);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 422);
        }
    }
}
