<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Lumen\Auth\Authorizable;
use Symfony\Component\Mime\Encoder\Base64Encoder;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
        'api_token',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @return BelongsToMany
     */
    public function companies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class, 'users_has_companies', 'user_id', 'company_id');
    }

    /**
     * @param $request
     * @return mixed
     */
    static public function createUser($request)
    {
        return User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
        ]);
    }

    /**
     * @param $request
     * @return array
     */
    static public function checkUser($request): array
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return [
                'status' => 'user not found'
            ];
        }

        if (!Hash::check($request->password, $user->password)) {
            return [
                'status' => 'wrong password'
            ];
        }

        $authToken = (new Base64Encoder())->encodeString(Str::random(8));

        User::where('email', $request->email)->update(['api_token' => $authToken]);

        return [
            'status' => 'success',
            'api_token' => $authToken
        ];
    }

    /**
     * @param $request
     * @return string[]
     */
    static public function recoverUserPassword($request): array
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return [
                'status' => 'user not found'
            ];
        }

        // I show in order to be able to test that everything works.
        // Since the new password is not sent to the mail
        $newPassword = Str::random(8);
        $user->update(['password' => Hash::make($newPassword)]);

        return [
            'status' => 'success',
            'user' => $user,
            'password' => $newPassword
        ];
    }
}
