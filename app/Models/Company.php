<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Company extends Model
{
    use HasFactory;

    protected $table = 'companies';

    protected $fillable = [
        'title',
        'phone',
        'description',
    ];

    /**
     * @return BelongsTo
     */
    public function users(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $request
     * @param $user
     * @return mixed
     */
    static public function saveNewCompany($request, $user)
    {
        $addNewCompany = Company::create([
            'title' => $request->title,
            'phone' => $request->phone,
            'description' => $request->description ?? ''
        ]);

        return $user->companies()->save($addNewCompany);
    }
}
