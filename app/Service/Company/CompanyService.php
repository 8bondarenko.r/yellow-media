<?php

namespace App\Service\Company;

use App\Models\Company;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CompanyService
{
    /**
     * @param $request
     * @return JsonResponse
     */
    public function storeCompany($request): JsonResponse
    {
        $user = User::findOrFail(Auth::user()->id);

        $rules = [
            'title' => 'string|required',
            'phone' => 'string|required',
            'description' => 'string'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $addNewCompany = Company::saveNewCompany($request, $user);

        return response()->json([
            'status' => 'success',
            'user' => $user,
            'company' => $addNewCompany
        ]);
    }

    /**
     * @param $userId
     * @return JsonResponse
     */
    public function getCompanyByUserId($userId): JsonResponse
    {
        $user = User::findOrFail($userId);

        return response()->json([
            'status' => 'success',
            'user' => $user,
            'company' => $user->companies->toArray()
        ]);
    }
}
