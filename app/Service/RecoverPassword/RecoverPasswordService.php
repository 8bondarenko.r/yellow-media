<?php

namespace App\Service\RecoverPassword;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class RecoverPasswordService implements InterfaceRecoverPassword
{
    /**
     * @param $request
     * @return JsonResponse
     */
    public function recoverPassword($request): JsonResponse
    {
        $rules = [
            'email' => 'required|email',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ], 422);
        }

        return response()->json(User::recoverUserPassword($request));
    }
}
