<?php

namespace App\Service\RecoverPassword;

interface InterfaceRecoverPassword
{
    /**
     * @param $request
     * @return mixed
     */
    public function  recoverPassword($request);
}
