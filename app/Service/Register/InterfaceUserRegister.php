<?php

namespace App\Service\Register;

interface InterfaceUserRegister
{
    /**
     * @param $request
     * @return mixed
     */
    public function register($request);
}
