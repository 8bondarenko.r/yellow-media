<?php

namespace App\Service\Register;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class RegisterService implements InterfaceUserRegister
{
    /**
     * @param $request
     * @return JsonResponse
     */
    public function register($request): JsonResponse
    {
        $rules = [
            'first_name' => 'string|required',
            'last_name' => 'string|required',
            'email' => 'required|email|unique:users',
            'password' => 'string|required|min:8',
            'phone' => 'string|required|unique:users',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ], 422);
        }

        return response()->json(User::createUser($request));
    }
}
