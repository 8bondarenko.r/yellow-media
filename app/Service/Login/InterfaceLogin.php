<?php

namespace App\Service\Login;

interface InterfaceLogin
{
    /**
     * @param $request
     * @return mixed
     */
    public function  login($request);
}
