<?php

namespace App\Service\Login;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class LoginService implements InterfaceLogin
{
    /**
     * @param $request
     * @return JsonResponse
     */
    public function login($request): JsonResponse
    {
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return response()->json([
                'error' => $validator->errors()
            ], 422);
        }

        return response()->json(User::checkUser($request));
    }
}
