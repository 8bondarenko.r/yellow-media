--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: companies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.companies (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    phone character varying(255) NOT NULL,
    description text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.companies OWNER TO postgres;

--
-- Name: companies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.companies_id_seq OWNER TO postgres;

--
-- Name: companies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.companies_id_seq OWNED BY public.companies.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    phone character varying(255) NOT NULL,
    api_token character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_has_companies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users_has_companies (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    company_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users_has_companies OWNER TO postgres;

--
-- Name: users_has_companies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_has_companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_has_companies_id_seq OWNER TO postgres;

--
-- Name: users_has_companies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_has_companies_id_seq OWNED BY public.users_has_companies.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: companies id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies ALTER COLUMN id SET DEFAULT nextval('public.companies_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: users_has_companies id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_has_companies ALTER COLUMN id SET DEFAULT nextval('public.users_has_companies_id_seq'::regclass);


--
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.companies (id, title, phone, description, created_at, updated_at) FROM stdin;
1	Labadie-Nitzsche	+1-769-337-3453	Reprehenderit molestiae aliquam laudantium sapiente expedita numquam. Neque omnis accusamus recusandae fugiat.	2022-06-23 12:13:54	2022-06-23 12:13:54
2	Heidenreich-Abshire	+1-657-809-5405	Aspernatur delectus neque maxime sint. Enim est et velit modi velit odit molestiae. Quia repellat soluta et vel eligendi.	2022-06-23 12:13:54	2022-06-23 12:13:54
3	Daniel Inc	605.507.9946	Suscipit est aut voluptatem. Nam nisi quia et at et. Maiores eveniet recusandae laboriosam eaque.	2022-06-23 12:13:54	2022-06-23 12:13:54
4	Gerhold-Crona	1-678-802-4459	Aliquid ut sed nisi qui vitae aliquam. Est et possimus earum qui fugiat doloribus nobis. Aut quis tempora explicabo amet aut perferendis et.	2022-06-23 12:13:54	2022-06-23 12:13:54
5	Senger LLC	+1 (430) 483-6617	Optio velit cupiditate voluptas aut quam. Est ipsam enim magnam aut cumque. Rem vel sunt rerum cum est eos quaerat sed.	2022-06-23 12:13:54	2022-06-23 12:13:54
6	Rogahn Inc	+12342447325	Rerum esse architecto eius nisi est iure perspiciatis qui. Minima temporibus illum eos velit odio quia. Voluptas doloremque repellendus enim molestias.	2022-06-23 12:13:54	2022-06-23 12:13:54
7	Haley, Parisian and Leannon	(856) 500-0815	Deleniti harum aspernatur labore quia consectetur aut quas. Nostrum et doloremque exercitationem. Deserunt est ea quis ut pariatur pariatur ut nihil.	2022-06-23 12:13:54	2022-06-23 12:13:54
8	Reichel Group	+1 (928) 870-0962	Vel ut asperiores quo suscipit nisi esse. Deserunt fugit qui architecto accusamus at eaque. Doloribus recusandae molestiae autem officiis voluptate. Expedita aut tempora veritatis ullam perferendis.	2022-06-23 12:13:54	2022-06-23 12:13:54
9	Bauch Ltd	+1 (513) 326-2134	Error id dolorum similique explicabo provident velit. Placeat provident dolorem exercitationem. Corrupti reprehenderit assumenda ut et. Minima debitis expedita minima eos et.	2022-06-23 12:13:54	2022-06-23 12:13:54
10	Pfeffer, Schoen and Stokes	+1.401.203.3939	Aperiam eligendi qui veritatis rem numquam voluptatem ea. Doloribus aut eaque maxime provident nesciunt. Alias id omnis enim reiciendis et quibusdam. Quae adipisci necessitatibus libero iusto modi consequatur.	2022-06-23 12:13:54	2022-06-23 12:13:54
11	Hessel LLC	+1-785-282-4693	Est itaque rerum sunt qui. Debitis qui dignissimos asperiores eveniet dicta vitae amet. Qui nisi maxime necessitatibus non dolore.	2022-06-23 12:13:54	2022-06-23 12:13:54
12	Brekke LLC	815.340.7160	Officiis repellendus earum enim qui eaque est voluptatum. Commodi dolorum quia quo autem sed eveniet. Nisi quo earum ipsa est rerum aliquam iure. Quia placeat minus quos rerum nulla velit.	2022-06-23 12:13:54	2022-06-23 12:13:54
13	Ferry and Sons	(253) 742-9576	Aut est numquam laudantium. Reprehenderit dolor ullam natus expedita. Nemo perspiciatis rerum quisquam explicabo fuga. Nisi et distinctio sed ducimus.	2022-06-23 12:13:54	2022-06-23 12:13:54
14	Kertzmann-Boyle	1-585-489-5123	Est tempore dolor eaque enim eos quia necessitatibus molestias. Nemo voluptatem unde quasi et dolorem facere. Exercitationem odio architecto qui vitae harum laudantium.	2022-06-23 12:13:54	2022-06-23 12:13:54
15	Ziemann, Hane and Macejkovic	272.570.1010	Tempora quia sint dolor. Nobis aut architecto eum et. Labore eos voluptatibus sed omnis soluta error.	2022-06-23 12:13:54	2022-06-23 12:13:54
16	Grimes, Auer and Mitchell	+1 (630) 800-0801	Id voluptas labore eos. Similique rerum aut iure incidunt ut.	2022-06-23 12:13:54	2022-06-23 12:13:54
17	Sawayn-Gusikowski	+1-210-415-7716	Perspiciatis ipsum ipsa nisi quidem enim rerum facere velit. Ut nihil impedit quo architecto. Accusamus eum labore et voluptas accusamus dolor eveniet. Quis et eum id vero laudantium.	2022-06-23 12:13:54	2022-06-23 12:13:54
18	Stoltenberg and Sons	(469) 830-7966	Animi eaque ut minima magni quae placeat soluta. Ex sunt praesentium sint. Ut rerum voluptatem facilis dolore inventore consectetur.	2022-06-23 12:13:54	2022-06-23 12:13:54
19	Purdy, Dickinson and Wilderman	1-725-950-1844	Magni quo voluptate ex commodi eum. Cumque soluta est nostrum sint sequi unde voluptas. Exercitationem magni asperiores corporis.	2022-06-23 12:13:54	2022-06-23 12:13:54
20	Altenwerth, Skiles and Larson	+1 (380) 669-8448	Doloribus iste consectetur et quos facere adipisci tempore. Voluptas ex voluptatibus eos omnis consequatur deserunt. At repudiandae aut nam itaque et voluptatem ducimus. Ut ducimus assumenda omnis aut est.	2022-06-23 12:13:54	2022-06-23 12:13:54
21	Pagac, Ferry and Tillman	762.646.9520	Aliquid incidunt et ut. Voluptas ea sint et nobis. Ad nemo est mollitia nulla.	2022-06-23 12:13:54	2022-06-23 12:13:54
22	Schneider-Witting	334-857-8584	Eos consequatur commodi itaque corporis optio. Magnam esse nisi eos totam maiores ut magnam. Molestias cum nulla libero et nihil. Voluptas voluptas quia non est officia et quisquam.	2022-06-23 12:13:54	2022-06-23 12:13:54
23	Armstrong Ltd	+1.850.823.7910	Non aut cumque rerum ullam. Accusantium et excepturi dolor iure. Sequi eveniet et et cum ratione dolor facere. Ducimus neque eaque est velit consequatur expedita molestiae.	2022-06-23 12:13:54	2022-06-23 12:13:54
24	Hand, Gerhold and Bayer	386.238.6389	Aliquid dolores quos corrupti magnam impedit eos qui quos. Eveniet in quia quos atque voluptas. Eos temporibus consectetur nihil fugit harum id voluptate et. Et placeat mollitia dignissimos perferendis voluptatem.	2022-06-23 12:13:54	2022-06-23 12:13:54
25	Kertzmann-Nitzsche	(757) 205-2121	Omnis hic dolore qui voluptas. Nihil quis accusamus fuga aliquid dolorem aut ea. Eveniet asperiores rerum velit quos sit neque voluptates. Est qui cupiditate consequuntur dolor.	2022-06-23 12:13:54	2022-06-23 12:13:54
32	Company abibas	778912-45487-444	Top abibas	2022-06-23 17:59:30	2022-06-23 17:59:30
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
34	2022_06_22_183251_create_user_table	1
35	2022_06_22_183327_create_companies_table	1
36	2022_06_22_183747_create_users_has_companies_table	1
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, first_name, last_name, email, password, phone, api_token, created_at, updated_at) FROM stdin;
1	Odell	Pfeffer	bvon@example.com	$2y$10$qeJTxv3PB7T8Y4ifmJZj8OZzQNXVhpMx4FhdZpRYnHaSKzDNJUuWS	(760) 904-8495	bE9tZzlOeHQ1ajFUOVNadm11RGprWEJFMnpmbDhjYUxPcFc3NUxXcE9PdUNjMjlRcDhNcVNoNTNu\r\nMTdhWkNJeg==	2022-06-23 12:13:54	2022-06-23 12:13:54
2	Marcel	Olson	lloyd.klocko@example.org	$2y$10$9Ls8I0yfUPp/.W.I6utlAeoymwBeR4AXRgL6qfQX20g3ErcfAGIKW	(401) 363-5610	OFdTVWFYMmxmR21oOUZMODVMMFNISjNmVGVVQW1EaEltOEdiS2pPaTRqaDJMamltUDlocDVFbkNk\r\nNHQxOG13Rg==	2022-06-23 12:13:54	2022-06-23 12:13:54
3	Elise	Goodwin	swalker@example.com	$2y$10$.0nzz2wYBfM19RBD6q..S.r3oQPy7ub6UuIFKAQ4vIuiiv6pLcAlu	270.873.3258	TzM5TmxZZ0FHWFJjdHg4dmlHTDJuck93M2RsdllVekM1WnhWZXNHTlNRWmlaZnN0dzU2TlVVUVl2\r\nUVlRUHR6Yw==	2022-06-23 12:13:54	2022-06-23 12:13:54
4	Leanne	Schneider	macey88@example.net	$2y$10$jvC9is0U7owwQz3rsH3BkOtQg9y/hSJY5NGHbrKSkG639cJOmGV96	1-313-933-8444	MEVzTW84Wm1qaUQ1U29oOXdZRzUyRWdVcUFCeWlUOHRNeVN1Mmx2R3Brdmw2aGl4dGJOellWMjRt\r\nb3BsZjNlRA==	2022-06-23 12:13:54	2022-06-23 12:13:54
5	Elmore	Walter	serenity.strosin@example.com	$2y$10$q4dmHb2.c2Ac.4dwTYeFR.TpqlgzIO4v5jGYC1xbq1Cjh8gIFWrLG	+1 (240) 337-1275	VklnWXN3dGpCdXpjbVlvZzdUMTI3eFQxWXVUdnd4SXoxUnpKSWEyMDBiWVNDcUtoeU9ZYmxMU1dt\r\nQldUcm4yRA==	2022-06-23 12:13:54	2022-06-23 12:13:54
17	asdashjghjg	asdasd	sdfsdf@ua.fm	$2y$10$6IwxqFdwxdSrTbpYEq8/7uNFsu824cj0WIngglq5N5ucUTrCX.f0.	423423423423	Qm1uOVAyekw3OHp2bUNqSVFhZkxjdHVqRmhSOFFxRk0=	2022-06-23 12:18:44	2022-06-23 12:18:44
20	asdashjghjg	asdasd	sdfsdf@ua.fm1	$2y$10$cSs5Np.V2f2Qy95wPjFhfOqwIA6UDXW8ukZKmRAV8Ift4ktsNgkHO	4234234234231	RGpSTzNzd1RVOG1kS2I3Vjh6N2pjSkRYaXRVbDlxWEpRdmdFUnRjVlJBSUZoWkRNa3IxcDhOQjNZ\r\nb2NyaGM4dA==	2022-06-23 12:19:56	2022-06-23 12:19:56
21	test	TEst2	tes@email.com	$2y$10$QQEJn5oe3a82dElAOufJt.HIuYj5XA5mAPL6G7eUkeVrKGq8XEYOO	423411111111	cDl4RkhaVmZBTWJJRjJIZ1hVMGdvZUlLcEVxWFREb1JBWkk0VUZaZTlENDB4MEJBNHdCb3h5cXBt\r\nbTRTdFZOYQ==	2022-06-23 12:23:25	2022-06-23 12:23:25
22	test	TEst2	tes@email.com11	$2y$10$pcUcsj.wpq8F2RtJdE8YzOWtiZ5IoS5J06g2Iq/feARF3bLfoLei2	423411111111	RExqSk5IN2NBRWdOU3JES2FpQ011QUxYYTFuZGFHdDdXelJGSmpXZkRnaG9YNVJHcU92OGZFZ0w3\r\nUjhnRU9YYQ==	2022-06-23 12:34:02	2022-06-23 12:34:02
24	test	TEst2	tes@email.com111	$2y$10$mnzMwpmoppKQk9HUUbG65efaduT6qa9EvCdMClXgy5YizMyFXIi5i	4234111111114	S1lNbGcwRVExc2VSTUI2dUw4Z3prelFEUHMzcHFTNEVsbGcycmh2VjFxNWNHQTgzSFVYeUNEdVMw\r\nU2VhN1pHUg==	2022-06-23 12:40:12	2022-06-23 12:40:12
28	Admin	Adminovich	admin@gmail.com	$2y$10$7zGGFIR8NMDmQBTV0qney.Dv80EQH68eJkYs3TSAG0JF2t.A84kbe	0990993310	RHVRTTQ2d0s=	2022-06-23 17:58:59	2022-06-23 17:59:13
\.


--
-- Data for Name: users_has_companies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users_has_companies (id, user_id, company_id, created_at, updated_at) FROM stdin;
1	1	1	\N	\N
2	1	2	\N	\N
3	1	3	\N	\N
4	1	4	\N	\N
5	1	5	\N	\N
6	2	6	\N	\N
7	2	7	\N	\N
8	2	8	\N	\N
9	2	9	\N	\N
10	2	10	\N	\N
11	3	11	\N	\N
12	3	12	\N	\N
13	3	13	\N	\N
14	3	14	\N	\N
15	3	15	\N	\N
16	4	16	\N	\N
17	4	17	\N	\N
18	4	18	\N	\N
19	4	19	\N	\N
20	4	20	\N	\N
21	5	21	\N	\N
22	5	22	\N	\N
23	5	23	\N	\N
24	5	24	\N	\N
25	5	25	\N	\N
26	28	26	\N	\N
31	28	32	\N	\N
\.


--
-- Name: companies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.companies_id_seq', 32, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 36, true);


--
-- Name: users_has_companies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_has_companies_id_seq', 31, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 30, true);


--
-- Name: companies companies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users_has_companies users_has_companies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_has_companies
    ADD CONSTRAINT users_has_companies_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_has_companies users_has_companies_company_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_has_companies
    ADD CONSTRAINT users_has_companies_company_id_foreign FOREIGN KEY (company_id) REFERENCES public.companies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users_has_companies users_has_companies_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users_has_companies
    ADD CONSTRAINT users_has_companies_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

