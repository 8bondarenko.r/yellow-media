### Structure
```
app
    Http
        Controllers    
                API
                    CompanyController.php
                    UserLoginController.php
                    UserRecoverPasswordController.php
                    UserRegisterController.php
    Service
        Company
            CompanyService.php
        Login
            LoginService.php
        RecoverPassword
            RecoverPasswordService.php
        Register  
            RegisterService.php  
                    
```
### route file
```
routes
    api.php
```    

### files
```
Yellow Api-test.postman_collection.json
yellow_test_db.sql
```
### .env
```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=yellow_test_db
DB_USERNAME=postgres
DB_PASSWORD=
```

