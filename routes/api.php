<?php
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['namespace' => 'API', 'prefix' => 'api', 'as' => 'api.'], function () use ($router) {
    $router->group(['prefix' => 'user'], function () use ($router) {
        $router->post('register', ['as' => 'register', 'uses' => 'UserRegisterController@register']);
        $router->post('sign-in', ['as' => 'sign-in', 'uses' => 'UserLoginController@authenticate']);
        $router->post('recover-password', ['as' => 'recover-password', 'uses' => 'UserRecoverPasswordController@recoverPassword']);

        $router->group(['middleware' => 'auth:api'], function () use ($router) {
            $router->post('company', ['as' => 'company', 'uses' => 'CompanyController@store']);
            $router->get('company', ['as' => 'company', 'uses' => 'CompanyController@show']);
        });
    });
});
